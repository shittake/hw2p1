'''Farmer_Fox.py
by Jin Terada White
UWNetID: jintw
Student number: 1924679

Assignment 2, in CSE 415, Winter 2021.
 
This file contains my problem formulation for the problem of
the Farmer, Fox, Chicken, and Grain.
'''

#<METADATA>
SOLUZION_VERSION = "2.0"
PROBLEM_NAME = "Farmer, Fox, Chicken, and Grain"
PROBLEM_VERSION = "1.0"
PROBLEM_AUTHORS = ['Jin Terada White']
PROBLEM_CREATION_DATE = "18-JAN-2021"

# The following field is mainly for the human solver, via either the Text_SOLUZION_Client.
# or the SVG graphics client.
PROBLEM_DESC=\
 '''The <b>"Farmer, Fox, Chicken, and Grain"</b> problem is a traditional puzzle
in which the player starts off with A Farmer, Fox, Chicken, and bag of Grain on
the left side of a river and must use a boat to transport everything, includinh himself,
to the other side of the bank. However, if the Fox and Chicken are without the farmer together, the
Fox will eat the Chicken. If the Chicken and Grain are without the farmer together then 
the Chicken will eat the grain.
'''
#</METADATA>

#<COMMON_DATA>
#</COMMON_DATA>
#<COMMON_CODE>
NAMES=["Farmer", "Fox", "Chicken", "Grain"] #Names of each array index
F=0 # array index to access farmer boolean
FX=1 # same for fox
C=2 # same for chicken
G=3 # same for grain

class State():

    # initialize the state of the problem
    def __init__(self, state=None):
        if state==None:
            state = [True,True,True,True]
        self.state = state
    
    def __eq__(self,s2):
        return [i for i in self.state] == [i for i in s2.state]

    def __str__(self):
        #Produce text desc. of a state
        left = [NAMES[i] if c else "" for c, i in zip(self.state, range(0,len(self.state)))]
        right = [NAMES[i] if left[i] == '' else '' for i in range(0,len(left))]
        return ' '.join(left).replace('  ', ' ') + " <--> " + ' '.join(right).replace('  ', ' ')

    def __hash__(self):
        return (self.__str__()).__hash__()

    # returns a copy of the state
    def copy(self):
        return State([p for p in self.state])

    # Tests whether it's legal to move the farmer and p passenger to the other side
    def can_move(self, p):
        test = self.copy()
        test_state = test.state
        test_state[F] = not test_state[F]
        if p != F:
            test_state[p] = not test_state[p]
        # conditions to return false
        to_left = test_state[F] and ((not test_state[FX] and not test_state[C]) or (not test_state[C] and not test_state[G]))
        to_right = not test_state[F] and ((test_state[FX] and test_state[C]) or (test_state[C] and test_state[G]))
        return not (to_left or to_right)

    # operator to move farmer and passenger p (p is the Farmer is no passenger) to other side
    def move(self, p):
        news = self.copy()
        news.state[F] = not news.state[F]
        if p != F:
            news.state[p] = not news.state[p]
        return news
    
def goal_test(s):
    return all(p == False for p in s.state)

def goal_message(s):
    return "You got everyone to the other side safely! Good job :)"
        
class Operator:
    def __init__(self, name, precond, state_transf):
        self.name = name
        self.precond = precond
        self.state_transf = state_transf

    def is_applicable(self, s):
        return self.precond(s)

    def apply(self, s):
        return self.state_transf(s)

#</COMMON_CODE>

#<INITIAL_STATE>
CREATE_INITIAL_STATE = lambda : State()
#CREATE_INITIAL_STATE = None
#</INITIAL_STATE>

#<OPERATORS>
trip_combinations = [F, FX, C, G]

OPERATORS = [Operator(
  "Move Farmer across" if p == F else "Move Farmer and " + NAMES[p] + " across",
  lambda s, p1=p: s.can_move(p1),
  lambda s, p1=p: s.move(p1) ) 
  for (p) in trip_combinations]
#</OPERATORS>

#<GOAL_TEST> (optional)
GOAL_TEST = lambda s: goal_test(s)
#</GOAL_TEST>

#<GOAL_MESSAGE_FUNCTION> (optional)
GOAL_MESSAGE_FUNCTION = lambda s: goal_message(s)
#</GOAL_MESSAGE_FUNCTION>
